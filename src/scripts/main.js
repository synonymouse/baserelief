(function($) {
	/**
	 * Application singleton
	 */
	window.App = (function() {
		// Default App config
		var DEFAULTS = {};
		var instance;
		var App = function(config) {
			// Singleton duty
			if (instance !== undefined) {
				throw new Error('App has already instantiated');
			}
			instance = this;
			// Extend config
			$.extend(this.config, DEFAULTS, config);
			// Execute polyfill for external SVG
			svg4everybody();
		};
		App.getInstance = function() {
			return instance;
		};
		$.extend(App.prototype, {
			initCommon: function() {
				$('.tabs').tabs();
				$('.scrolling-nav .frame').hiddenScroller();
				this.initMegaMenu();
				this.initMainSlider();
				this.initStore();
			},
			initMegaMenu: function() {
				// build mega-menu map {parent id -> $menu}
				var menus = {};
				$('.mega-menu').each(function() {
					var id = $(this).data('parent');
					if (id === undefined || id === null || id === '') {
						throw new Error('Some of mega-menus has no data-parent');
					}
					menus[id] = $(this);
				});
				
				// Register mouse enter|leave handlers for primary nav anchors
				var $primaryAnchors = $('.primary-nav a').hover(function() {
					var id = $(this).parent().data('id');
					if (id) {
						openMegaMenu(id);
						unsetMegaMenuDefferedClosing(id);
					} else {
						closeAnyMegaMenu();
					}
				}, function() {
					var id = $(this).parent().data('id');
					if (id) {
						closeMegaMenuDeferred(id);
					}
				});
				// Register clicking handlers
				var clickFromAnchor = false;
				$primaryAnchors.click(function(e) {
					e.preventDefault();
					clickFromAnchor = true;
					var id = $(this).parent().data('id');
					if (id) {
						openMegaMenu(id);
					}
				});
				var clickFromMegaMenu = false;
				$('.mega-menu')
					.click(function() {
						clickFromMegaMenu = true;
					})
					.hover(function() {
						var id = $(this).data('parent');
						unsetMegaMenuDefferedClosing(id);
					}, function() {
						var id = $(this).data('parent');
						closeMegaMenuDeferred(id);
					});
				$(document).click(function(e) {
					// Check the click isn't from mega-menu or a .primary-nav anchor
					if (!clickFromAnchor && !clickFromMegaMenu) {
						closeAnyMegaMenu();
					}
					// Reset click types
					clickFromAnchor = false;
					clickFromMegaMenu = false;
				});

				function getMegaMenu(id) {
					if (!menus.hasOwnProperty(id)) {
						throw new Error('There is no mega-menu for #' + id);
					}
					return menus[id];
				}

				function openMegaMenu(id) {
					if (isMegaMenuOpen(id))
						return;
					closeAnyMegaMenu({animate: false});
					var $menu = getMegaMenu(id);
					$menu.addClass('open').hide().slideDown(180, function() {
						$(this).css('display', '');
					});
					// Activate .primary-nav's item
					$primaryAnchors.parent()
						.filter('[data-id="' + id + '"]')
						.addClass('active');
				}

				function closeMegaMenuDeferred(id) {
					var $menu = getMegaMenu(id);
					var timeoutId = $menu.data('closeTimeoutId');
					if (!timeoutId) {
						timeoutId = setTimeout(function() {
							closeMegaMenu(id);
							$menu.removeData('closeTimeoutId');
						}, 1000);
						$menu.data('closeTimeoutId', timeoutId);
					}
				}

				function unsetMegaMenuDefferedClosing(id) {
					var $menu = getMegaMenu(id);
					var timeoutId = $menu.data('closeTimeoutId');
					if (timeoutId) {
						clearTimeout(timeoutId);
						$menu.removeData('closeTimeoutId');
					}
				}

				// Closes any open mega menus
				function closeAnyMegaMenu(options) {
					Object.getOwnPropertyNames(menus).forEach(function(id) {
						closeMegaMenu(id, options);
					});
				}

				function closeMegaMenu(id, options) {
					if (!isMegaMenuOpen(id))
						return;
					var $menu = getMegaMenu(id);
					$menu.removeClass('open');
					options = $.extend({}, {animate: true}, options);
					if (options.animate) {
						$menu.show().slideUp(150, function() {
							$(this).css('display', '');
						});
					}
					// Disactivate .primary-nav's item
					$primaryAnchors.parent()
						.filter('[data-id="' + id + '"]')
						.removeClass('active');
				}

				function isMegaMenuOpen(id) {
					return getMegaMenu(id).hasClass('open');
				}
			},
			initMainSlider: function() {
				var $slider = $('.main-slider .slides').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: false,
					fade: false,
					mobileFirst: true,
					autoplay: true,
					autoplaySpeed: 3000,
					responsive: [
						{
							breakpoint: 1200,
							settings: {
								arrows: true
							}
						},
					]
				});
			},
			initStore: function() {
				$('.store .tabs').hiddenScroller();
			}
		})
		return App;
	})();
})(jQuery);

/**
 * $.tabs() plugin
 * 
 * It's not like $.tabs() in bootstrap, don't confuse with $.tabpanes()
 *
 * @author Alexander Korostin <coderlex@gmail.com>
 */
(function($) {
	var Tabs = function($host) {
		this.$host = $($host);
	}

	$.extend(Tabs.prototype, {
		init: function() {
			this.$host.on('click', 'a', $.proxy(this.onAnchorClick, this));
		},
		onAnchorClick: function(e) {
			e.preventDefault();
			var $targetItem = $(e.currentTarget).parent();
			// Already here, do nothing
			if ($targetItem.hasClass('active')) {
				return;
			}
			// Deactivate previous tab
			var $relatedItem = $targetItem.siblings().filter('.active');
			$relatedItem.removeClass('active');
			// Activate current tab
			$targetItem.addClass('active');
		}
	});

	$.fn.tabs = function() {
		return this.each(function() {
			var $host = $(this);
			if (!$host.data('__tabs')) {
				var tabs = new Tabs($host);
				tabs.init();
				$host.data('__tabs', tabs);
			}
		});
	};
})(jQuery);

/**
 * $.hiddenScroller() plugin
 *
 * @author Alexander Korostin <coderlex@gmail.com>
 */
(function($) {
	// Adding some easing functions
	$.extend($.easing, {
		easeOutQuint: function (x, t, b, c, d) {
			return c*((t=t/d-1)*t*t*t*t + 1) + b;
		}
	});

	var DEFAULTS = {
		wrapContent: false,
		cursor: 'move',
		swipeInertia: 100,
		inertialExtent: 80
	};

	var HiddenScroller = function($host, options) {
		this.$host = $host;
		this.options = $.extend({}, DEFAULTS, options);
		this.startedPan = false;
	}

	$.extend(HiddenScroller.prototype, {
		init: function() {
			if (this.options.wrapContent) {
				this.$wrapper = $('<div/>');
				this.$host.children().remove().appendTo(this.$wrapper)
				this.$wrapper.appendTo($host)
			} else {
				this.$wrapper = this.$host.children();
			}

			this.$host.css({
				overflow: 'hidden'
			});
			this.$wrapper.css({
				position: 'relative',
				left: '0px'
			});

			this.relayout();

			$(window).resize($.proxy(this.relayout, this));

			var hammer = new Hammer(this.$host[0]);
			hammer.on('panstart', $.proxy(function(e) {
				if (this.options.swipeInertia) {
					// stop swipe inertia animation
					this.scrollToX(this.getScrollX(), {
						animate: false,
						jumpToEnd: false
					});
				}
				this.startScrollX = this.getScrollX();
				this.startedPan = true;
			}, this));
			hammer.on('panleft panright', $.proxy(function(e) {
				if (this.startedPan) {
					var scrollX = this.clampScrollX(this.startScrollX - e.deltaX);
					this.scrollToX(scrollX, {animate: false});
				}
			}, this));
			hammer.on('panend pancancel', $.proxy(function(e) {
				this.startedPan = false;

				// Add swipe inertia
				if (this.options.swipeInertia) {
					var inertialX = -e.velocityX * this.options.swipeInertia;
					var scrollX = this.clampScrollX(this.getScrollX() + inertialX);
					this.scrollToX(scrollX, {
						duration: 400,
						easing: 'easeOutQuint',
						// Correct position if it will be out of bound after
						// animation ends
						complete: $.proxy(this.relayout, this)
					});
				}

				if (this.options.inertialExtent) {
					this.relayout();
				}
			}, this));
		},
		clampScrollX: function(scrollX) {
			// prepare limits scrollX will clamp to
			var minScrollX = 0;
			var maxScrollX = this.getMaxScrollX();
			if (this.options.inertialExtent) {
				minScrollX -= this.options.inertialExtent;
				maxScrollX += this.options.inertialExtent;
			}
			// clamp to [minScrolX, maxScrollX]
			scrollX = Math.max(minScrollX, scrollX);
			scrollX = Math.min(maxScrollX, scrollX);
			return scrollX;
		},
		getMaxScrollX: function() {
			return Math.max(this.getOverflowSpaceX(), 0);
		},
		getScrollX: function() {
			return -parseInt(this.$wrapper.css('left'));
		},
		scrollToX: function(x, options) {
			options = $.extend({}, {
				animate: true,
				enqueue: false,
				jumpToEnd: true,
				duration: 100,
				easing: 'swing',
				complete: undefined
			}, options);

			this.$wrapper.stop(!options.enqueue, options.jumpToEnd);
			var css = { left: -x + 'px' };
			if (options.animate) {
				this.$wrapper.animate(css, options.duration, options.easing, options.complete);
			} else {
				this.$wrapper.css(css);
			}
		},
		getOverflowSpaceX: function() {
			return this.$wrapper.outerWidth(true) - this.$host.width();
		},
		relayout: function() {
			if (this.options.cursor) {
				if (this.getOverflowSpaceX() > 0) {
					this.$host.css({
						cursor: this.options.cursor
					});
				} else {
					this.$host.css({
						cursor: ''
					});
				}
			}
			// Correct scroller position if it got out of bounds
			var freeSpaceLeft = -this.getScrollX();
			var freeSpaceRight = this.getScrollX() - this.getOverflowSpaceX();
			if (freeSpaceLeft > 0) {
				this.scrollToX(0, {duration: 150});
			} else if (freeSpaceRight > 0) {
				this.scrollToX(this.getMaxScrollX(), {duration: 150});
			}
		}
	});

	$.fn.hiddenScroller = function(options) {
		return this.each(function() {
			var $host = $(this);
			if (!$host.data('__hidden-scroller')) {
				var scroller = new HiddenScroller($host, options);
				scroller.init();
				$host.data('__hidden-scroller', scroller);
			}
		});
	};
})(jQuery);