'use strict'

const gulp            = require('gulp'),
      concat          = require('gulp-concat'),
      gutil           = require('gulp-util'),
      cleanDest       = require('gulp-clean-dest'),
      spritesmith     = require('gulp.spritesmith'),
      sass            = require('gulp-sass'),
      sourcemaps      = require('gulp-sourcemaps'),
      imageDataURI    = require('gulp-image-data-uri'),
      path            = require('path'),
      runSequence     = require('run-sequence'),
      modernizr       = require('modernizr'),
      fs              = require('fs'),
      svgstore        = require('gulp-svgstore'),
      svgmin          = require('gulp-svgmin'),
      ejs             = require('gulp-ejs'),
      uglify          = require('gulp-uglifyjs'),
      rename          = require('gulp-rename'),
      connect         = require('gulp-connect')

const DEBUG = ['prod', 'production'].indexOf(process.env.NODE_ENV) < 0

/**
 * Build UI texture atlas
 */
gulp.task('sprite:ui', () => {
	return gulp.src('src/images/sprites/ui/*.png')
		.pipe(spritesmith({
			imgName: 'dist/img/ui.png',
			imgPath: '../img/ui.png',
			cssName: 'src/styles/sprites/_ui.scss',
			imgOpts: {quality: 75},
			cssVarMap: sprite => {
				sprite.name = 'sprite-' + sprite.name;
				//console.log(sprite)
			}
		}))
		.pipe(gulp.dest('.'))
})

/**
 * Build all texture atlases
 */
gulp.task('sprite', ['sprite:ui'])

/**
 * Build SASS -> CSS
 */
gulp.task('sass', () => {
	let main = gulp.src('src/styles/main.scss')
	if (DEBUG) {
		main = main.pipe(sourcemaps.init())
	}
	main = main.pipe(sass({
			outputStyle: 'compressed',
			includePaths: [
				__dirname + '/node_modules/foundation-sites/scss'
			],
			errLogToConsole: true
		})
		.on('error', sass.logError))
	if (DEBUG) {
		main = main.pipe(sourcemaps.write('./maps'))
	}
	// Remove piped files so that if there was an error
	// their old versions wont exist.
	return main.pipe(cleanDest('dist/css'))
		.pipe(gulp.dest('dist/css'))
		.pipe(connect.reload())
})

/**
 * Creates SASS mixins with data-uri from raster images.
 */
gulp.task('data-uri:bitmaps', () => {
	const globs = ['*.jpeg', '*.jpg', '*.png', '*.gif']
	return gulp.src(globs.map(glob => path.join('src/images/data-uri', glob)))
		.pipe(imageDataURI({
			template: {
				file: './src/styles/data-uri/_bitmaps.scss.mustache',
			}
		}))
		.pipe(concat('_bitmaps.scss'))
		.pipe(gulp.dest('src/styles/data-uri'))
})

/**
 * Creates SASS mixins with data-uri from SVG images.
 */
gulp.task('data-uri:svg', () => {
	const globs = ['*.svg']
	return gulp.src(globs.map(glob => path.join('src/images/data-uri', glob)))
		.pipe(imageDataURI({
			template: {
				file: './src/styles/data-uri/_svg.scss.mustache',
			}
		}))
		.pipe(concat('_svg.scss'))
		.pipe(gulp.dest('src/styles/data-uri'))
})

/**
 * Base64-encode all vector and raster images to data-uri in SASS partial
 */
gulp.task('data-uri', ['data-uri:bitmaps', 'data-uri:svg'])

/**
 * Generates the modernizr.js containing preconfigured modernizer test code.
 */
gulp.task("modernizr", callback => {
	modernizr.build({
		'minify': false,
		'options': [
			'setClasses', 'addTest', 'mq'
		],
		"feature-detects": [
			'css/transitions', 'css/transforms', 'css/transforms3d', 'css/animations',
			'css/flexbox', 'css/backgroundsize', 'css/gradients'
		]
	}, function (result) {
		//const wrappedResult = result + '\nmodule.exports = window.Modernizr;'
		fs.writeFile(__dirname + "/src/scripts/modernizr.js", result, function(err) {
			if(err) {
				console.log(err)
			}
			callback()
		})
	})
})

/**
 * Combines svg files into one with <symbol> elements.
 * Read more about this here: http://css-tricks.com/svg-symbol-good-choice-icons/
 */
gulp.task('svgstore:ui', () => {
	return gulp.src('src/images/svgstore/ui/*.svg')
		.pipe(svgmin(file => {
			var prefix = path.basename(file.relative, path.extname(file.relative));
			return {
				plugins: [{
					cleanupIDs: {
						prefix: prefix + '-',
						minify: true
					}
				}],
				js2svg: {
					pretty: true
				}
			}
		}))
		.pipe(svgstore())
		.pipe(gulp.dest('dist/img'))
})

/**
 * Combines svg files into one with <symbol> elements.
 */
gulp.task('svgstore', ['svgstore:ui'])

/**
 * Builds EJS templates to HTML
 */
gulp.task('ejs', () => {
	function entab(text, tabCount) {
		const tab = '\t'.repeat(tabCount)
		return text.split(/\r?\n/)
			.map(line => tab + line)
			.join('\n')
			.replace(/^\t+/, '')
	}
	let stub = JSON.parse(fs.readFileSync(__dirname + '/stub.json'))
	return gulp.src('src/templates/*.ejs')
		.pipe(ejs(Object.assign({}, stub, {
			entab
		}), {
			ext: '.html'
		}).on('error', gutil.log))
		.pipe(gulp.dest('./dist'))
		.pipe(connect.reload())
})

/**
 * Copy images from src into dist folder.
 */
gulp.task('images:copy', () => {
	return gulp.src([
			'src/images/**/*',
			'!src/images/data-uri',
			'!src/images/data-uri/**/*',
			'!src/images/sprites',
			'!src/images/sprites/**/*',
			'!src/images/raw',
			'!src/images/raw/**/*',
			'!src/images/svgstore',
			'!src/images/svgstore/**/*'
		])
		.pipe(gulp.dest('dist/img'))
})

/**
 * Copy web fonts.
 */
gulp.task('fonts:copy', () => {
	return gulp.src('src/fonts/*/**')
		.pipe(gulp.dest('dist/fonts'))
})

/**
 * Copy js scripts.
 */
gulp.task('js:minify', () => {
	return gulp.src([
			'src/scripts/modernizr.js',
			'src/scripts/main.js'
		])
		.pipe(uglify('app.min.js', {
			outSourceMap: true
		}))
		.pipe(gulp.dest('dist/js'))
		.pipe(connect.reload())
})

gulp.task('watch', function() {
	connect.server({
		name: 'Dist App',
		root: 'dist',
		port: 8080,
		livereload: true
	});
	gulp.watch('src/styles/**/*.scss', ['sass'])
	gulp.watch('src/templates/**/*', ['ejs'])
	gulp.watch('src/scripts/**/*', ['js:minify'])
});

gulp.task('default', cb => {
	runSequence(
		['fonts:copy', 'js:minify', 'images:copy'],
		[/*'svgstore',*/ 'sprite', 'data-uri'],
		['sass', 'modernizr', 'ejs'],
		cb)
})